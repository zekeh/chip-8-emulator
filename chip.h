class chip8 {
public:
	chip8();
	~chip8();
	void initialize();
	void emulateCycle();
	bool loadGame(const char * filename);
	bool drawFlag;
	unsigned char gfx[64 * 32]; //graphics and sound
	unsigned char key[16]; //inputs
private:
	unsigned char memory[4096];
	unsigned char V[16];
	unsigned short I; //index register
	unsigned short pc; //program counter
	unsigned short opcode;
	unsigned char delay_timer;
	unsigned char sound_timer; //both count down from 60 hertz
	unsigned short stack[16]; //store return addresses of subroutines
	unsigned short sp; //stack pointer
};
