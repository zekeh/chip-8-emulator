#include <iostream>
#include "SDL.h"
#include "chip.h"
using namespace std;

char keys[16] = {
	SDLK_x,
	SDLK_1,
	SDLK_2,
	SDLK_3,
	SDLK_q,
	SDLK_w,
	SDLK_e,
	SDLK_a,
	SDLK_s,
	SDLK_d,
	SDLK_z,
	SDLK_c,
	SDLK_4,
	SDLK_r,
	SDLK_f,
	SDLK_v,
};

//key mapping
void keyInput(chip8 &chip8){
	SDL_Event e;
	while (SDL_PollEvent(&e)) {
		if (e.type == SDL_QUIT)
			exit(0);
		
		int x = 1;
		if (e.type == SDL_KEYUP)
			x = 0;
		for (int i = 0; i < 16; ++i) {
			if (e.key.keysym.sym == keys[i]) {
				chip8.key[i] = x;
			}
		}
	}
}

int main(int argc, char **argv) {

	chip8 chip8;

	int WIDTH = 640;
	int HEIGHT = 320;
	SDL_Window* window = nullptr;

	if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
		cout << "error: " << SDL_GetError();
		return -1;
	}

	window = SDL_CreateWindow("CHIP-8", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
		WIDTH, HEIGHT, SDL_WINDOW_SHOWN);
	if (window == nullptr) {
		cout << "error" << SDL_GetError();
		return -1;
	}

	SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);

	SDL_Texture *texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ARGB8888,
		SDL_TEXTUREACCESS_STREAMING, 64, 32);

	if (!chip8.loadGame(argv[1]))
		return -1;

	// Game loop
	while (true) {
		chip8.emulateCycle();

		keyInput(chip8);

		int *pixels;
		int pitch;

		SDL_LockTexture(texture, nullptr, (void **)&pixels, &pitch);
		for (int i = 0; i < 2048; ++i) {
			pixels[i] = 0xFFFFFFFF * chip8.gfx[i];
		}
		SDL_UnlockTexture(texture);

		SDL_RenderCopy(renderer, texture, nullptr, nullptr);

		SDL_RenderPresent(renderer);

		}
	}